let App = (function (axios, Mustache) {	
	let genres = [];
	let movies = [];	

	// dom element with $
	let $genres = document.getElementById('genres');
	let $movies = document.getElementById('movies');
	// added
	let $infoWindow = document.getElementById('window');

	let genresTpl = document.getElementById('genres-template').innerHTML;
	let moviesTemplate = document.getElementById('movies-template').innerHTML;	
	
	//console.log(movieListing);

	return {		
		start: function () {
			let self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				let data = response.data;
				genres = data.genres;
				movies = data.movies;
				self.createLinks();
			});
		},
		fetch: function (cb) {
			return axios.get('data/db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		updateMovies: function () {
			//console.log(movies[1].genres[1]);
			// TODO: compile movies template;
			console.log(window.location.hash);
			console.log(window.location.hash.slice(2));
			console.log(movies.length);
			console.log(movies[1].title);

			let myGenre = window.location.hash.slice(2);
			let listOfMovies = [];
			for (let i=0; i<movies.length; i++) {
				console.log(movies[i].title);
				if (movies[i].genres.includes(myGenre)) {
					listOfMovies.push(movies[i])
				}
			}
			$movies.innerHTML = Mustache.render(moviesTemplate, {
				movies:listOfMovies
			});
		},
		getMovieById: function(idClick) {
			let aMovie = movies[idClick];
			console.log(movies[idClick]);
			
			let parameter = '<input class="btn" id="close" type="button" value="close">';
			parameter += '<p class="infopar"><span class="info">Database ID: </span>' + aMovie.id + '<br>';
			parameter += '<span class="info">Title: </span>' + aMovie.title + '<br>';
			parameter += '<span class="info">Year: </span>' + aMovie.year + '<br>';
			parameter += '<span class="info">Runtime: </span>' + aMovie.runtime + '<br>';
			parameter += '<span class="info">Genre: </span>' + aMovie.genres.join(", ") + '<br>';
			parameter += '<span class="info">Director: </span>' + aMovie.director + '<br>';
			parameter += '<span class="info">Actors: </span>' + aMovie.actors + '<br>';
			parameter += '<span class="info">Plot: </span>' + aMovie.plot + '<br>';
			parameter += '<img class="image2" src="' + aMovie.posterUrl + '"alt="poster"></p>';
			$infoWindow.innerHTML = parameter;
			
		},
		// add class 'showme' and display info window
		infoMovie: function(idClick) {
			console.log($infoWindow);
			$infoWindow.classList.add('showme');
			this.getMovieById(idClick);
		},
		// close window with movie info
		closeInfo: function() {
			$infoWindow.classList.remove('showme');
		},
		// check click and decide what to do with it
		checkClick: function(e) {
			console.log(e.target.id);			
			if (e.target.id.slice(0, 2) === 'no' || e.target.id.slice(0, 2) === 'nu') {
				// extract string, convert to number and reduce 1 (array starts at 0)
				let idClick = Number(e.target.id.slice(2)) - 1;
				//alert('true');
				this.infoMovie(idClick);
			}
			if (e.target.id === 'close') {
				//alert('it is working');
				this.closeInfo();
			}
		},
		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));
			window.addEventListener('click', this.checkClick.bind(this));
		}
	}
})(axios, Mustache);
